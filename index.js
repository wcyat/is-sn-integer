"use strict";
const is_number = require("is-number");
/**
 * Returns true if the given number is an integer
 * @param num - The number to check.
 */
module.exports = function (num) {
  return is_number(num) && Number.isInteger(Number(num));
};
