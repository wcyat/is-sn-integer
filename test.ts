import isInteger from "./index";
import { exit } from "process";
const truelist = ["1", -1, "2", 0b110010, -0x96, "0x1e0", "-11", 80, "-002"];
const falselist = [NaN, undefined, "-1.5", -3.2, "0.510", "000.455"];
for (const i of truelist) {
  if (!isInteger(i)) {
    console.error(`isInteger(${i}) returned "false" but should return "true".`);
    exit(1);
  }
}
for (const i of falselist) {
  if (isInteger(i)) {
    console.error(`isInteger(${i}) returned "true" but shoud return "false".`);
    exit(1);
  }
}
console.log("success");
