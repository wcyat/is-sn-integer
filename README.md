# is-sn-integer

Check if a string or a number is an integer.

## Install

```bash
npm install is-sn-integer
```

## Usage

### CommonJS

```javascript
const isInteger = require('is-sn-integer');
```

### Module

```javascript
import isIinteger from 'is-sn-integer';
```

## Output

```javascript
isInteger("1")         //true
isInteger("1.1")       //false
isInteger("2")         //true
isInteger("10.5")      //false
isInteger(1)           //true
isInteger(10.1)        //false
isInteger(NaN)         //false
isInteger(undefined)   //false
```
